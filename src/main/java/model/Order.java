package model;

import java.time.LocalDateTime;

public class Order {

    private int id;
    private String reference;
    private LocalDateTime dateOfOrder;
    private String comment;
}
