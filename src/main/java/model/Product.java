package model;

public class Product {

    private int id;
    private String name;
    private String reference;
    private String description;
    private float unitPriceBeforeTax;
    private float taxRate;
    private int quantityAvailable;
    private boolean isSellable;
}
